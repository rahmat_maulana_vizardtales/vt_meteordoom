﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ObjectController : MonoBehaviour
{

    GameplayManager gm;
    public int ID;

    // Start is called before the first frame update
    void Start()
    {
        gm = FindObjectOfType<GameplayManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!gm.pause)
        {
            if (ID == 0)
            {
                transform.Translate(new Vector2(0, gm.AsteroidSpeed * Time.deltaTime));
            }
            else if (ID == 1)
            {
                transform.Translate(new Vector2(0, gm.bg1Speed * Time.deltaTime));
            }
            else if (ID == 2)
            {
                transform.Translate(new Vector2(0, gm.bg2Speed * Time.deltaTime));
            }
            if (transform.position.y >= 6.9f)
            {
                Destroy(gameObject);
            }
        }
    }
}

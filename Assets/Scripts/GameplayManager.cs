﻿/*created by Rahmat MPR*/
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{

    [Header("Increment Component")]
    public float ScoreInc = 0.1f;
    public float AsteroidSpeed = 2.0f;
    public float bg1Speed = 2.5f;
    public float bg2Speed = 1.0f;
    [Space(15)]

    GameManager Gm;
    public bool start = false;
    public bool GameOver = false;
    public int Score = 0;
    public int BestScore;
    public Text ScoreText;
    public Text LoseScoreText;
    public GameObject newBestScore;

    public GameObject LosePanel;

    public bool MainMenu = false;
    public bool pause = false;


    // Start is called before the first frame update
    void Start()
    {
        if (!pause)
            if (!MainMenu)
            {
                Gm = FindObjectOfType<GameManager>();
                if (Gm != null)
                    BestScore = Gm.BestScore;
                StartCoroutine(incrementScore());
                StartCoroutine(IncrementScoreCond());
            }
    }

    // Update is called once per frame
    void Update()
    {
        if (!pause)
            if (!MainMenu)
            {
                if (!GameOver)
                    ScoreText.text = "" + Score + "";

                if (GameOver)
                {
                    checktheScore();
                    LosePanel.SetActive(true);
                }
            }
    }

    public void setPause(bool set)
    {
        pause = set;
    }
    public void checktheScore()
    {
        if (Score > BestScore)
        {
            ScoreText.gameObject.SetActive(false);
            LoseScoreText.text = ""+Score+"";
            BestScore = Score;
            Gm.saveBestScore(BestScore);
            newBestScore.SetActive(true);


        }
        else
        {

            ScoreText.gameObject.SetActive(false);
            LoseScoreText.text = "" + Score + "";
            newBestScore.SetActive(false);
        }
    }

    IEnumerator incrementScore()
    {
        
            while (!GameOver)
            {
            yield return new WaitForSeconds(ScoreInc);
            Score += 1;
            }
        
    }

    IEnumerator IncrementScoreCond()
    {
        if (!GameOver)
        {
            bool stop = false;
            while (!stop)
            {
                yield return new WaitForSeconds(30);
                Debug.Log("incremented");
                AsteroidSpeed += 0.5f;
                bg1Speed += 0.5f;
                bg2Speed += 0.5f;
                if (ScoreInc > 0.01f)
                {
                    ScoreInc -= 0.01f;
                }
            }
        }
    }
}

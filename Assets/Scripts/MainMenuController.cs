﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MainMenuController : MonoBehaviour
{
    public Text ScoreText;
    GameManager gm;
    // Start is called before the first frame update
    void Start()
    {
        gm = FindObjectOfType<GameManager>();
        gm.loadBestScore();
        loadTheScore();
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    public void loadTheScore()
    {
        ScoreText.text = ""+gm.BestScore+"";
    }

    public void SetSound(float val)
    {
        gm.setSound(val);
    }
}

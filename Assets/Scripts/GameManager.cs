﻿/*created by Rahmat MPR*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
	public static GameManager instance;
	public int BestScore;
	public AudioSource SfxAu,BGM;
	public float SoundVal;

	//check this instance
	void Awake()
	{
		if (instance == null)
		{
			DontDestroyOnLoad(this.gameObject);
			instance = this;
		}

		else if (instance != this)
		{
			Destroy(gameObject);
			return;
		}

	}
	// Start is called before the first frame update
	void Start()
    {
		loadBestScore();
		loadSound();
	}

	// Update is called once per frame
	void Update()
    {
		if (Input.GetKeyDown(KeyCode.Escape) || Input.backButtonLeavesApp)
		{
			saveSound();
			Application.Quit();
		}
	}

	//load The Best Score
	public void loadBestScore()
    {
		BestScore = PlayerPrefs.GetInt("BestScore", 0);
	}

	//save the Best Score
	public void saveBestScore(int val)
    {
		PlayerPrefs.SetInt("BestScore",val);
    }

    public void loadSound()
    {
		SoundVal = PlayerPrefs.GetFloat("Sound", 1);
		SfxAu.volume = SoundVal;
		BGM.volume = SoundVal;
	}
	public void saveSound()
    {
		PlayerPrefs.SetFloat("Sound", SoundVal);
	}

	public void setSound(float set)
    {
		SoundVal = set;
		SfxAu.volume = SoundVal;
		BGM.volume = SoundVal;
	}
    public void playSFX()
    {
		SfxAu.Play();
    }
}
